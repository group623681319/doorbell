#!/usr/bin/env python
from importlib import import_module
import os
from flask import Flask, render_template, Response
import time
import grovepi
import sys
import dbhandler
from datetime import datetime
import MySQLdb
import graph
import threading
import requests
from bs4 import BeautifulSoup
import shutil

app = Flask(__name__, template_folder='templateFiles', static_folder='staticFiles')
ultrasonic = 4
grovepi.set_bus("RPI_1")

light_sensor = 0
led = 3

# Turn on LED once sensor exceeds threshold resistance
threshold = 50

grovepi.pinMode(light_sensor,"INPUT")
grovepi.pinMode(led,"OUTPUT")




# Fetching image links from the cloud 
def htmlParser():
    html = requests.get("http://139.162.233.153:8001/")
    
    soup = BeautifulSoup(html.content, "html.parser")
    
    classes = soup.find_all('a')
    links = ["http://139.162.233.153:8001/"+ link.get('href').encode("ascii") for link in classes]
    
    try:
        shutil.rmtree("/home/pi/Final Build/flask-video-streaming/staticFiles/recent/")
    except:
        print("Directory remove failed")
    os.mkdir("/home/pi/Final Build/flask-video-streaming/staticFiles/recent/")
    for link in links:
        img_data = requests.get(link).content
        path = "/home/pi/Final Build/flask-video-streaming/staticFiles/recent/" + link.split('/')[3]
        with open (path, 'wb') as handler:
            print(path)
            handler.write(img_data)
            handler.close()
    filenames = [x.split('/')[3] for x in links]
    return filenames


@app.route('/')
def index():
    try:
        links = htmlParser()
    except:
        print("Failed")
        
    """Video streaming home page."""
    gh = graph.GraphHandler()
    gh.run()
    print(os.path.abspath(os.getcwd()))
    
    return render_template('index.html',links=links)

# Saving image frame and uploading to the cloud 
def saveFrame(frame):
    time = datetime.today().strftime('%H:%M:%S')
    filename = time + '.jpg'
    with open(filename,'wb') as file:
        file.write(frame)
    
    file.close()
    dbhandler.uploadImage(filename)
    #making an entry to the database on the cloud
    dbhandler.addEntry(filename)
    
    os.remove(filename)
    

def gen():
    """Video streaming generator function."""
    yield b'--frame\r\n'
    while True:
        time.sleep(0.1)
        try: 
            with open('/home/pi/frame.jpg', 'rb') as frame:
                yield b'Content-Type: image/jpeg\r\n\r\n' + frame.read() + b'\r\n--frame\r\n'
            frame.close()
        except KeyboardInterrupt:
            return
        except Exception as e:
            print(e)
            continue


@app.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')



"""
This segment deals with the display
"""

if sys.platform == 'uwp':
    import winrt_smbus as smbus
    bus = smbus.SMBus(1)
else:
    import smbus
    import RPi.GPIO as GPIO
    rev = GPIO.RPI_REVISION
    if rev == 2 or rev == 3:
        bus = smbus.SMBus(1)
    else:
        bus = smbus.SMBus(0)

# this device has two I2C addresses
DISPLAY_RGB_ADDR = 0x62
DISPLAY_TEXT_ADDR = 0x3e

# set backlight to (R,G,B) (values from 0..255 for each)
def setRGB(r,g,b):
    bus.write_byte_data(DISPLAY_RGB_ADDR,0,0)
    bus.write_byte_data(DISPLAY_RGB_ADDR,1,0)
    bus.write_byte_data(DISPLAY_RGB_ADDR,0x08,0xaa)
    bus.write_byte_data(DISPLAY_RGB_ADDR,4,r)
    bus.write_byte_data(DISPLAY_RGB_ADDR,3,g)
    bus.write_byte_data(DISPLAY_RGB_ADDR,2,b)

# send command to display (no need for external use)    
def textCommand(cmd):
    bus.write_byte_data(DISPLAY_TEXT_ADDR,0x80,cmd)

# set display text \n for second line(or auto wrap)     
def setText(text):
    textCommand(0x01) # clear display
    time.sleep(.05)
    textCommand(0x08 | 0x04) # display on, no cursor
    textCommand(0x28) # 2 lines
    time.sleep(.05)
    count = 0
    row = 0
    for c in text:
        if c == '\n' or count == 16:
            count = 0
            row += 1
            if row == 2:
                break
            textCommand(0xc0)
            if c == '\n':
                continue
        count += 1
        bus.write_byte_data(DISPLAY_TEXT_ADDR,0x40,ord(c))

#Update the display without erasing the display
def setText_norefresh(text):
    textCommand(0x02) # return home
    time.sleep(.05)
    textCommand(0x08 | 0x04) # display on, no cursor
    textCommand(0x28) # 2 lines
    time.sleep(.05)
    count = 0
    row = 0
    while len(text) < 32: #clears the rest of the screen
        text += ' '
    for c in text:
        if c == '\n' or count == 16:
            count = 0
            row += 1
            if row == 2:
                break
            textCommand(0xc0)
            if c == '\n':
                continue
        count += 1
        bus.write_byte_data(DISPLAY_TEXT_ADDR,0x40,ord(c))

setText("Door locked")

@app.route('/unlock')
def unlock():
    setRGB(0,255,0)
    setText("Door unlocked")
    return render_template('index.html')



@app.route('/lock')
def lock():
    setRGB(255,0,0)
    setText("Door locked")
    return render_template('index.html')


def scanSensor():
    flag = True
    while True:
        time.sleep(0.1)
        distance = grovepi.ultrasonicRead(ultrasonic)

        if distance < 30:
            try:
                # Get sensor value
                sensor_value = grovepi.analogRead(light_sensor)
                # Calculate resistance of sensor in K
                if sensor_value <= 0:
                    sensor_value = 5
                resistance = (float)(1023 - sensor_value) * 10 / sensor_value
                distance = grovepi.ultrasonicRead(ultrasonic)

                if resistance > threshold and distance < 30:
                    # Send HIGH to switch on LED
                    grovepi.digitalWrite(led,1)
                else:
                    # Send LOW to switch off LED
                    grovepi.digitalWrite(led,0)

            except IOError:
                print ("Error")

            if flag:
                print('Uploading Picture')
                frame = open('/home/pi/frame.jpg', 'rb').read()
                saveFrame(frame)
                flag = False
        else:
            flag = True

if __name__ == '__main__':
    thread2 = threading.Thread(target=scanSensor)
    thread2.start()
    app.run(host='0.0.0.0', threaded=True)
    thread2.join()
