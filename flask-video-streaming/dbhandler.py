
import MySQLdb
from datetime import datetime
import requests

db = MySQLdb.connect(
    host="139.162.233.153",
    user="doorbell2",
    password="password",
    database="doorbell"
)

cursor = db.cursor()

url = 'http://139.162.233.153:8000'

def uploadImage(filename):
    file = open(filename,'rb') 
    headers = {
        'Content-Type': 'image/jpeg'
    }
    requests.post(url,data = file, headers=headers)
    print('Uploaded image')
        
def addEntry(filename):
    statement = "INSERT INTO Motion_Log (date,time,url) VALUES (%s,%s,%s)"
    date = datetime.today().strftime('%Y-%m-%d')
    time = datetime.today().strftime('%H:%M:%S')
    vals = (date,time,filename)
    cursor.execute(statement,vals)
    db.commit()
    print('Inserted...' + date + ' ' + time)


