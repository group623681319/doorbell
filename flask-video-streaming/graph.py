import MySQLdb
import matplotlib.pyplot as plt
import time

import matplotlib
matplotlib.use('Agg')

    
data = []

daysOccurances = [['Monday', 0], ['Tuesday', 0], ['Wednesday', 0], ['Thursday', 0], ['Friday', 0], ['Saturday', 0], ['Sunday', 0]]

timeOccurances = [[x,0] for x in range(24)]
    


def column(matrix, i):
    return [row[i] for row in matrix]


class GraphHandler():
    
    def getMotionData(self):
        db = MySQLdb.connect(
            host = '139.162.233.153',
            user= 'doorbell2',
            password = 'password',
            database = 'doorbell'
        )

        cursor = db.cursor()
        statement = 'SELECT * FROM Motion_Log'
        cursor.execute(statement)
        res = cursor.fetchall()
        cursor.close()
        return [x for x in res]



    def run(self):
        data = self.getMotionData()
        print('xxxx')


        for x in data:
                date = x[0]
                time = str(x[1])
                hour = int(time.split(':')[0])
                daysOccurances[date.weekday()][1] += 1
                timeOccurances[hour][1] += 1


        plt.bar(column(daysOccurances,0),column(daysOccurances,1),facecolor='black')

        plt.title("Motion Detected For Each Day Of The Week")
        plt.xlabel("Day")
        plt.ylabel("Count")

        plt.savefig('/home/pi/Final Build/flask-video-streaming/staticFiles/daysofweek.png')
        
        plt.close()
        

        plt.plot(column(timeOccurances,0),column(timeOccurances,1))

        plt.title("Motion Detected For Each Hour Of The Day")
        plt.xlabel("Hour")
        plt.ylabel("Count")

        plt.savefig('/home/pi/Final Build/flask-video-streaming/staticFiles/hoursofday.png')
        
        plt.close()

#        plt.close()
