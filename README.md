# Doorbell

Clone the project as it is.

Run the 'startup.sh' :
-- This bash file will execute both 'facial recognition' and the 'server' hosting the website

By default the server is routed to localhost:5000

In order to go public, 'ngrok' can be implemented which is quite easy to do and from there the localhost can be tunnelled to a public address.